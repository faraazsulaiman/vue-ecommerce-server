package com.faraazs.vueecommerceserver.modules.product;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

/**
 * @author Faraaz Sulaiman
 * @since 2019-11-16
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TestProductController
{
	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void testProductCreation() throws Exception
	{
		String name = "Test product";
		String desc = "This is a product that will be used for test";
		String imageUrl = "https://picsum.photos/600/300/?image=25";
		double price = 250.50;

		Product p = new Product();
		p.setName(name);
		p.setDescription(desc);
		p.setImageUrl(imageUrl);
		p.setPrice(price);

		ResponseEntity<Product> productResponseEntity = this.restTemplate.postForEntity("http://localhost:" + port + ProductController.BASE_PATH, p, Product.class);
		Product body = productResponseEntity.getBody();
		Assert.isTrue(name.equalsIgnoreCase(body.getName()), "Invalid name");
		Assert.isTrue(desc.equalsIgnoreCase(body.getDescription()), "Invalid description");
		Assert.isTrue(imageUrl.equalsIgnoreCase(body.getImageUrl()), "Invalid Image URL");
		Assert.isTrue(price == body.getPrice(), "Invalid Price");
	}
}
