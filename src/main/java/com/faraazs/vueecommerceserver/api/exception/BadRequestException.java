package com.faraazs.vueecommerceserver.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadRequestException extends Exception
{

	/**
	 * Instantiates a new Resource not found exception.
	 *
	 * @param message the message
	 */
	public BadRequestException(String message)
	{
		super(message);
	}
}
