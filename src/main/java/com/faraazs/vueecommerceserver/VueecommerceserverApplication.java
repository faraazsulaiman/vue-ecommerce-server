package com.faraazs.vueecommerceserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class VueecommerceserverApplication
{

	public static void main(String[] args)
	{
		SpringApplication.run(VueecommerceserverApplication.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer()
	{
		return new WebMvcConfigurer()
		{
			@Override
			public void addCorsMappings(CorsRegistry registry)
			{
				registry.addMapping("/api/v1/product/*").allowedOrigins("http://localhost:8080");
				registry.addMapping("/api/v1/product").allowedOrigins("http://localhost:8080");
				registry.addMapping("/api/v1/user/*").allowedOrigins("http://localhost:8080");
				registry.addMapping("/api/v1/user").allowedOrigins("http://localhost:8080");
			}
		};
	}
}
