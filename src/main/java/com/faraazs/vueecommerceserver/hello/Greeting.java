package com.faraazs.vueecommerceserver.hello;

/**
 * @author Faraaz Sulaiman
 * @since 2019-11-12
 */
public class Greeting {

	private final long id;
	private final String content;

	public Greeting(long id, String content) {
		this.id = id;
		this.content = content;
	}

	public long getId() {
		return id;
	}

	public String getContent() {
		return content;
	}
}
