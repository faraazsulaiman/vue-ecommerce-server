package com.faraazs.vueecommerceserver.modules.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Faraaz Sulaiman
 * @since 2019-11-14
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long>
{
	@Query("SELECT user FROM User user  WHERE user.emailAddress= (:emailAddress)")
	User findByEmailAddress(@Param("emailAddress") String emailAddress);
}
