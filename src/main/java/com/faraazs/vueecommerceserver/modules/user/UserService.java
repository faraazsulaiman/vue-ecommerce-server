package com.faraazs.vueecommerceserver.modules.user;

import com.faraazs.vueecommerceserver.api.exception.BadRequestException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * @author Faraaz Sulaiman
 * @since 2019-11-14
 */
@Service
public class UserService
{
	@Autowired
	private UserRepository userRepository;

	public User createUser(User user)
	{
		user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
		user.setCreateDate(new Date());
		user.setLastUpdate(new Date());
		userRepository.save(user);
		return user;
	}

	public User loadUserByEmailAndPassword(String emailAddress, String rawPassword) throws BadRequestException
	{
		User user = userRepository.findByEmailAddress(emailAddress);
		if (user == null)
			throw new BadRequestException("Invalid username or password.");

		boolean passwordValid = new BCryptPasswordEncoder().matches(rawPassword, user.getPassword());
		if (!passwordValid)
			throw new BadRequestException("Invalid username or password.");

		return user;
	}


}
