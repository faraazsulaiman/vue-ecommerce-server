package com.faraazs.vueecommerceserver.modules.user;

import com.faraazs.vueecommerceserver.api.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author Faraaz Sulaiman
 * @since 2019-11-14
 */
@RestController()
public class UserController
{
	public static final String BASE_PATH = "/api/v1/user";
	@Autowired
	private UserService userService;

	@PostMapping(BASE_PATH)
	public User register(@Valid @RequestBody User user)
	{
		return userService.createUser(user);
	}

	@GetMapping(BASE_PATH)
	public User login(@Param("emailAddress") String emailAddress, @Param("password") String password) throws BadRequestException
	{
		return userService.loadUserByEmailAndPassword(emailAddress, password);
	}
}
