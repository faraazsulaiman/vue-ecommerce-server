package com.faraazs.vueecommerceserver.modules.product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Faraaz Sulaiman
 * @since 2019-11-14
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long>
{
	@Query("SELECT product FROM Product product  WHERE product.statusId= (:statusId)")
	List<Product> loadByStatus(@Param("statusId") int statusId);

	@Query("SELECT product FROM Product product  WHERE product.id= (:id)")
	Product loadById(@Param("id") long id);
}
