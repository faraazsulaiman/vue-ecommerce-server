package com.faraazs.vueecommerceserver.modules.product;

import com.faraazs.vueecommerceserver.api.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Faraaz Sulaiman
 * @since 2019-11-14
 */
@RestController()
public class ProductController
{
	public static final String BASE_PATH = "/api/v1/product";

	@Autowired
	private ProductService productService;

	@PostMapping(BASE_PATH)
	public Product createProduct(@Valid @RequestBody Product product)
	{
		return productService.createProduct(product);
	}

	@GetMapping(BASE_PATH)
	public Product getProduct(@Param("id") long id)
	{
		return productService.loadById(id);
	}

	@GetMapping(BASE_PATH +"/all")
	public List<Product> getProducts()
	{
		return productService.loadAllActive();
	}
}
