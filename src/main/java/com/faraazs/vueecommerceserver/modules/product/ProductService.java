package com.faraazs.vueecommerceserver.modules.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author Faraaz Sulaiman
 * @since 2019-11-14
 */
@Service
public class ProductService
{
	@Autowired
	private ProductRepository productRepository;

	public Product createProduct(Product product)
	{
		product.setCreateDate(new Date());
		product.setLastUpdate(new Date());
		productRepository.save(product);
		return product;
	}

	public List<Product> loadAllActive()
	{
		return productRepository.loadByStatus(Product.STATUS_ACTIVE);
	}

	public Product loadById(long id)
	{
		return productRepository.loadById(id);
	}
}
